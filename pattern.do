onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider -height 50 CLK_NRES
add wave -noupdate /tb_pattern/clk_s
add wave -noupdate -divider -height 25 <NULL>
add wave -noupdate /tb_pattern/nres_s
add wave -noupdate -divider -height 50 DIPS
add wave -noupdate /tb_pattern/dip1_s
add wave -noupdate -divider -height 25 <NULL>
add wave -noupdate /tb_pattern/dip2_s
add wave -noupdate -divider -height 25 <NULL>
add wave -noupdate /tb_pattern/dip3_s
add wave -noupdate -divider -height 25 <NULL>
add wave -noupdate /tb_pattern/dip4_s
add wave -noupdate -divider -height 25 <NULL>
add wave -noupdate /tb_pattern/dip5_s
add wave -noupdate -divider -height 25 <NULL>
add wave -noupdate /tb_pattern/dip6_S
add wave -noupdate -divider -height 50 TB
add wave -noupdate /tb_pattern/di_s
add wave -noupdate -divider -height 25 <NULL>
add wave -noupdate /tb_pattern/mrk_s
add wave -noupdate -divider -height 25 <NULL>
add wave -noupdate /tb_pattern/do_s
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {174946 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {3413578 ps}
