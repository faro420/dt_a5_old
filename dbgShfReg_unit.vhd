--@authors: Mir Farshid Baha,2141801, mirfarshid.baha@haw-hamburg.de
--          Mehmet Cakir, 2195657, mehmet.cakir@haw-hamburg.de

-- clocked input module for a 2bit number with low reset

library work;
    use work.all;
    
library ieee;
    use ieee.std_logic_1164.all;
    use ieee.std_logic_arith.all;
    
entity dbgShfReg_unit is
    port(
        --inputs
        received : in std_logic_vector(1 downto 0);
        --outputs
        dip : out std_logic_vector( 1 downto 0 );
    );
end entity dbgShfReg_unit;

architecture beh of dbgShfReg_unit is

begin
    dip <= received;
end architecture beh;