--@authors: Mir Farshid Baha,2141801, mirfarshid.baha@haw-hamburg.de
--          Mehmet Cakir, 2195657, mehmet.cakir@haw-hamburg.de

-- 2bit FIFO shift debugger with 5 2bit outputs
library work;
    use work.all;
    
library ieee;
    use ieee.std_logic_1164.all;
    use ieee.std_logic_arith.all;
    
entity dbgShfReg is
    port(
        --inputs
        received : in std_logic_vector( 1 downto 0 );
        clk : in std_logic;
        nres : in std_logic;
        --outputs
        dip2 : out std_logic_vector( 1 downto 0 ); 
        dip3 : out std_logic_vector( 1 downto 0 );
        dip4 : out std_logic_vector( 1 downto 0 );
        dip5 : out std_logic_vector( 1 downto 0 );
        dip6 : out std_logic_vector( 1 downto 0 )
    );
end entity dbgShfReg;

architecture beh of dbgShfReg is
signal dip_cs : std_logic_vector ( 9 downto 0) := (others => '0');
signal dip_ns : std_logic_vector ( 9 downto 0);
signal clockPulse_s : std_logic := '0';
begin

shift:
process (clockPulse_s) is
variable dip_v : std_logic_vector(9 downto 0) := (others => '0');
begin
    dip_ns <= dip_cs;
    dip_v(7 downto 0) := dip_cs(9 downto 2);
	dip_v(9 downto 8) := received(1 downto 0);
	dip_ns <= dip_v;
end process shift;

reg:
process (clk) is
    begin
    if clk = '1' and clk'event then
        if nres = '0' then
            dip_cs <= (others => '0');
            clockPulse_s <= '0';
        else
            dip_cs <= dip_ns;
            clockPulse_s <= not clockPulse_s;
        end if;
    end if;
end process reg;

dip6 <= dip_cs(1 downto 0);
dip5 <= dip_cs(3 downto 2);
dip4 <= dip_cs(5 downto 4);
dip3 <= dip_cs(7 downto 6);
dip2 <= dip_cs(9 downto 8);
end architecture beh;