--@authors: Mir Farshid Baha,2141801, mirfarshid.baha@haw-hamburg.de
--          Mehmet Cakir, 2195657, mehmet.cakir@haw-hamburg.de

-- 2bit FIFO shift debugger with 5 2bit outputs
library work;
    use work.all;
    
library ieee;
    use ieee.std_logic_1164.all;
    use ieee.std_logic_arith.all;
    
entity dut is -- Device Under test
        port (
        -- debugging only
           dip1 : out std_logic_vector( 1 downto 0 ); -- "Data In" Plus 1 clock cycle (for debugging purpose only)
           dip2 : out std_logic_vector( 1 downto 0 ); -- "Data In" Plus 2 clock cycles (for debugging purpose only)
           dip3 : out std_logic_vector( 1 downto 0 ); -- "Data In" Plus 3 clock cycles (for debugging purpose only)
           dip4 : out std_logic_vector( 1 downto 0 ); -- "Data In" Plus 4 clock cycles (for debugging purpose only)
           dip5 : out std_logic_vector( 1 downto 0 ); -- "Data In" Plus 5 clock cycles (for debugging purpose only)
           dip6 : out std_logic_vector( 1 downto 0 ); -- "Data In" Plus 6 clock cycles (for debugging purpose only)
        --
           do : out std_logic_vector( 1 downto 0 ); -- Data Out
           mrk : out std_logic; -- MaRK of 1st data following pattern
        --
           di : in std_logic_vector( 1 downto 0 ); -- Data In
        --
           clk : in std_logic; -- CLocK
           nres : in std_logic -- Not RESet ; low active reset
        );--]port
end entity dut;

architecture beh of dut is
signal sent_s : std_logic_vector(1 downto 0) := "00";
    component input is
    port (
        di  : in std_logic_vector(1 downto 0);
        clk : in std_logic;
        nres  : in  std_logic;
        sent: out  std_logic_vector(1 downto 0)
    );
    end component input;
    for all: input use entity work.input(beh);
    
    component evaluation is
    port (
        di  : in std_logic_vector(1 downto 0);
        clk : in std_logic;
        nres : in std_logic;
        do  : out std_logic_vector(1 downto 0);
        mrk : out std_logic
    );
    end component evaluation;
    for all: evaluation use entity work.evaluation(mealy);
    
    component dbgShfReg is
    port(
        --inputs
        received : in std_logic_vector( 1 downto 0 );
        clk : in std_logic;
        nres : in std_logic;
        --outputs
        dip2 : out std_logic_vector( 1 downto 0 ); 
        dip3 : out std_logic_vector( 1 downto 0 );
        dip4 : out std_logic_vector( 1 downto 0 );
        dip5 : out std_logic_vector( 1 downto 0 );
        dip6 : out std_logic_vector( 1 downto 0 )
    );
    end component dbgShfReg;
    for all: dbgShfReg use entity work.dbgShfReg(beh);    
begin
    dip1 <= sent_s;
    dbg_i: dbgShfReg
        port map(
        --inputs
        received => sent_s,
        clk => clk,
        nres => nres,
        --outputs
        dip2 => dip2, 
        dip3 => dip3,
        dip4 => dip4,
        dip5 => dip5,
        dip6 => dip6
        );
    input_i: input
         port map(
            di => di,
            clk => clk,
            nres => nres,
            sent => sent_s
        );
    evaluation_i: evaluation
        port map(
            di => sent_s,
            clk => clk,
            nres => nres,
            do => do,
            mrk => mrk
        );
end architecture beh;