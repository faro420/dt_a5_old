--@authors: Mir Farshid Baha,2141801, mirfarshid.baha@haw-hamburg.de
--          Mehmet Cakir, 2195657, mehmet.cakir@haw-hamburg.de

-- test bench for the entity counter
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

library work;
use work.all;

entity tb_pattern is
end tb_pattern;

architecture beh of tb_pattern is
        -- debugging only
    signal dip1_s : std_logic_vector( 1 downto 0 );
    signal dip2_s:  std_logic_vector( 1 downto 0 ); 
    signal dip3_s :  std_logic_vector( 1 downto 0 );
    signal dip4_s :  std_logic_vector( 1 downto 0 );
    signal dip5_s :  std_logic_vector( 1 downto 0 ); 
    signal dip6_S :  std_logic_vector( 1 downto 0 ); 
        --
    signal do_s :  std_logic_vector( 1 downto 0 ); 
    signal mrk_s : std_logic; 
        --
    signal di_s:  std_logic_vector( 1 downto 0 ); 
	signal clk_s: std_logic;
	signal nres_s: std_logic;

    component sg is
        port (
        txData : out std_logic_vector( 1 downto 0 );   -- TxData send to DUT
        clk    : out std_logic;                        -- CLocK
        nres   : out std_logic                         -- Not RESet ; low active reset
        );
    end component sg;
    for all : sg use entity work.sg( beh );

    component dut is
	    port(
        -- debugging only
           dip1 : out std_logic_vector( 1 downto 0 ); -- "Data In" Plus 1 clock cycle (for debugging purpose only)
           dip2 : out std_logic_vector( 1 downto 0 ); -- "Data In" Plus 2 clock cycles (for debugging purpose only)
           dip3 : out std_logic_vector( 1 downto 0 ); -- "Data In" Plus 3 clock cycles (for debugging purpose only)
           dip4 : out std_logic_vector( 1 downto 0 ); -- "Data In" Plus 4 clock cycles (for debugging purpose only)
           dip5 : out std_logic_vector( 1 downto 0 ); -- "Data In" Plus 5 clock cycles (for debugging purpose only)
           dip6 : out std_logic_vector( 1 downto 0 ); -- "Data In" Plus 6 clock cycles (for debugging purpose only)
        --
           do : out std_logic_vector( 1 downto 0 ); -- Data Out
           mrk : out std_logic; -- MaRK of 1st data following pattern
        --
           di : in std_logic_vector( 1 downto 0 ); -- Data In
        --
           clk : in std_logic; -- CLocK
           nres : in std_logic -- Not RESet ; low active reset
        );
    end component;
    for all: dut use entity work.dut ( beh );

begin
	sg_i : sg
        port map (
        txData => di_s,
		clk => clk_s,
		nres => nres_s 
        );
    
    dut_i : dut
        port map (
		dip1 => dip1_s,
		dip2 => dip2_s,
		dip3 => dip3_s,
		dip4 => dip4_s,
		dip5 => dip5_s,
		dip6 => dip6_s,	
        do => do_s,		
        mrk => mrk_s,
		di => di_s,
		clk => clk_s,
		nres => nres_s
        );
end beh;