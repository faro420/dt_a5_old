--@authors: Mir Farshid Baha,2141801, mirfarshid.baha@haw-hamburg.de
--          Mehmet Cakir, 2195657, mehmet.cakir@haw-hamburg.de

-- evaluation module for 11 -> 11-> 00 -> 00 pattern

library work;
    use work.all;
    
library ieee;
    use ieee.std_logic_1164.all;
    use ieee.std_logic_arith.all;
    
entity evaluation is
    port (
        di  : in std_logic_vector(1 downto 0);
        clk : in std_logic;
        nres : in std_logic;
        do  : out std_logic_vector(1 downto 0);
        mrk : out std_logic
    );
end entity evaluation;

architecture moore of evaluation is
    signal di_cs : std_logic_vector(1 downto 0) := (others => '0');
    signal do_ns : std_logic_vector(1 downto 0);
    signal do_cs : std_logic_vector(1 downto 0) := (others => '0');
	signal mrk_cs: std_logic:= '0';
    signal state_cs: std_logic_vector(2 downto 0) := (others => '0');
	signal state_ns: std_logic_vector(2 downto 0);
begin
    automat:
	process(di_cs, state_cs) is
		variable state_v: std_logic_vector(2 downto 0);
    begin
		case state_cs is
			when "000" =>
				if di_cs = "11" then
					state_v := "001";
			    else
					state_v := "000";
			    end if;
				
			when "001" =>
				if di_cs = "11" then
					state_v := "010";
				else
					state_v := "000";
				end if;
			
			when "010" =>
				if di_cs = "11" then
					state_v := "010";
				elsif di_cs = "00" then
					state_v := "011";
				else
					state_v := "000";
				end if;
			
			when "011" =>
				if di_cs = "00" then
					state_v := "100";
				else
					state_v := "000";
				end if;
				
		    when others =>
				state_v := "000";
			end case;
		state_ns <= state_v;
		do_ns <= di_cs;
	end process automat;
	
	auswertung:
	process (state_cs) is
		variable mrk_v: std_logic;
	begin
		if state_cs = "100" then
			mrk_v := '1';
		else
			mrk_v := '0';
		end if;
		
		mrk_cs <= mrk_v;
	end process auswertung;
	
    reg:
    process (clk) is
    begin
        if clk = '1' and clk'event then
            if nres = '0' then
                di_cs <= "00";
                do_cs <= "00";
			    state_cs <= "000";
            else		
			    state_cs <= state_ns;
                di_cs <= di;
                do_cs <= do_ns;
            end if;
        end if;
    end process reg;  
    do <= do_cs;
	mrk<= mrk_cs;
end architecture moore;

architecture mealy of evaluation is
    signal di_cs : std_logic_vector(1 downto 0) := (others => '0');
    signal do_ns : std_logic_vector(1 downto 0);
    signal do_cs : std_logic_vector(1 downto 0) := (others => '0');
	signal mrk_cs: std_logic:= '0';
    signal state_cs: std_logic_vector(2 downto 0) := (others => '0');
	signal state_ns: std_logic_vector(2 downto 0);
begin
    automat:
	process(di_cs, state_cs) is
		variable state_v: std_logic_vector(2 downto 0);
		variable mrk_v: std_logic;
    begin
		mrk_v := '0';
		case state_cs is
			when "000" =>
				if di_cs = "11" then
					state_v := "001";
			    else
					state_v := "000";
			    end if;
				
			when "001" =>
				if di_cs = "11" then
					state_v := "010";
				else
					state_v := "000";
				end if;
			
			when "010" =>
				if di_cs = "11" then
					state_v := "010";
				elsif di_cs = "00" then
					state_v := "011";
				else
					state_v := "000";
				end if;
			
			when "011" =>
				if di_cs = "00" then
					state_v := "100"; mrk_v := '1';
				else
					state_v := "000";
				end if;
				
		    when others =>
				state_v := "000";
			end case;
		state_ns <= state_v;
		mrk_cs <= mrk_v;
		do_ns <= di_cs;
	end process automat;
	
    reg:
    process (clk) is
    begin
        if clk = '1' and clk'event then
            if nres = '0' then
                di_cs <= "00";
                do_cs <= "00";
			    state_cs <= "000";
            else		
			    state_cs <= state_ns;
                di_cs <= di;
                do_cs <= do_ns;
            end if;
        end if;
    end process reg;  
    do <= do_cs;
	mrk<= mrk_cs;
end architecture mealy;