--@authors: Mir Farshid Baha,2141801, mirfarshid.baha@haw-hamburg.de
--          Mehmet Cakir, 2195657, mehmet.cakir@haw-hamburg.de

-- evaluation module for 11 -> 11-> 00 -> 00 pattern

library work;
    use work.all;
    
library ieee;
    use ieee.std_logic_1164.all;
    use ieee.std_logic_arith.all;
    
entity evaluation is
    port (
        di  : in std_logic_vector(1 downto 0);
        clk : in std_logic;
        nres : in std_logic;
        do  : out std_logic_vector(1 downto 0);
        mrk : out std_logic
    );
end entity evaluation;

architecture beh of evaluation is
    signal di_cs : std_logic_vector(1 downto 0) := (others => '0');
    signal do_ns : std_logic_vector(1 downto 0);
    signal do_cs : std_logic_vector(1 downto 0) := (others => '0');
	signal mrk_cs: std_logic:= '0';
    signal indicator_cs: std_logic_vector(2 downto 0) := (others => '0');
	signal indicator_ns: std_logic_vector(2 downto 0);
begin
    automat:
    process(di_cs,indicator_cs) is
        variable indicator: std_logic_vector(2 downto 0) := (others => '0');
    begin
        case di_cs is
            when "11" =>
                case indicator is
                    when "000" => indicator := "001";
                    when "001" => indicator := "010";
                    when others => indicator:= "010";
                end case;
            when "00" =>      
                case indicator is
                    when "010" => indicator := "011";
                    when "011" => indicator:= "100"; 
                    when others => indicator := "000"; 
                end case;
            when others => indicator := "000";
        end case;
        do_ns <= di_cs;
    end process automat;
	
    detection:
	process(indicator_cs) is
	    variable mrk_v:std_logic;
    begin
		if indicator_cs = "100" then
		   mrk_v:= '1';
		else 
		   mrk_v := '0';
		end if;
	mrk_cs <= mrk_v;
    end process detection;
    
   

    reg:
    process (clk) is
    begin
        if clk = '1' and clk'event then
            if nres = '0' then
                di_cs <= "00";
                do_cs <= "00";
			    indicator_cs <= "000";
            else		
			    indicator_cs <= indicator_ns;
                di_cs <= di;
                do_cs <= do_ns;
            end if;
        end if;
    end process reg;  
    do <= do_cs;
	mrk<= mrk_cs;
end architecture beh;