--@authors: Mir Farshid Baha,2141801, mirfarshid.baha@haw-hamburg.de
--          Mehmet Cakir, 2195657, mehmet.cakir@haw-hamburg.de

-- clocked input module for a 2bit number with low reset

library work;
    use work.all;
    
library ieee;
    use ieee.std_logic_1164.all;
    use ieee.std_logic_arith.all;
    
entity input is
    port (
        di  : in std_logic_vector(1 downto 0);
        clk : in std_logic;
        nres  : in  std_logic;
        sent: out  std_logic_vector(1 downto 0)
    );
end entity input;

architecture beh of input is
    signal di_cs : std_logic_vector(1 downto 0) := (others => '0');
	signal sent_cs : std_logic_vector(1 downto 0) := (others => '0');
	signal sent_ns :  std_logic_vector(1 downto 0);
    signal clockPulse_s : std_logic := '0';

begin
process(clockPulse_s) is
    --variables belong here
    variable di_v : std_logic_vector(1 downto 0) := (others => '0');
    variable sent_v : std_logic_vector(1 downto 0) := (others => '0');
    begin
    --procedure belongs here
    di_v := di_cs;
    sent_v := di_v;
	sent_ns <= sent_v;
    end process;

    regCheck:
    process ( clk ) is
    begin
    if clk = '1' and clk'event then
        if nres = '0' then
            di_cs <= (others => '0');
            sent_cs <= (others => '0');
        else
            di_cs <= di;
			sent_cs <= sent_ns;
            clockPulse_s <= not clockPulse_s;
        end if;    
    end if;
    end process regCheck;

    sent <= sent_cs;
end architecture beh;